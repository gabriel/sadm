{ config, lib, pkgs, ... }:

with lib; {
  options.sadm.roles.k3s = { enable = mkEnableOption "Deploy a k3s server"; };

  config = mkIf config.sadm.roles.k3s.enable {
    services.k3s = {
      enable = true;
      extraFlags = "--disable local-storage";
    };

    # networking.firewall.allowedTCPPorts = [ 6443 ];
  };
}
