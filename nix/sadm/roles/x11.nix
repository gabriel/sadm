{ config, lib, pkgs, ... }:

with lib; {
  options.sadm.roles.x11 = {
    enable = mkEnableOption "Enable X11 with i3 as a window manager";
  };

  config = mkIf config.sadm.roles.x11.enable {
    services.xserver.enable = true;
    services.xserver.libinput.enable = true;
    services.xserver.windowManager.i3.enable = true;

    sound.enable = true;
    hardware.pulseaudio.enable = true;

    hardware.bluetooth.enable = true;
    services.blueman.enable = true;

    services.printing.enable = true;

    fonts.fonts = with pkgs; [
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      fira-code
      fira-code-symbols
    ];

    environment.sessionVariables = { TERMINAL = "xfce4-terminal"; };

    environment.systemPackages = with pkgs;
      let
        extensions = with vscode-extensions; [
          bbenoist.nix
          ms-python.python
          vscodevim.vim
        ];
        my-vscode-with-extensions =
          vscode-with-extensions.override { vscodeExtensions = extensions; };
      in [
        arandr
        dunst
        google-chrome
        libnotify
        my-vscode-with-extensions
        pavucontrol
        python3
        spotify
        syncplay
        vlc
        xfce.xfce4-terminal
      ];
  };
}

