{ config, lib, pkgs, ... }:

with lib; {
  options.sadm.roles.sysadmin = {
    enable = mkEnableOption "Miscellaneous configuration for CLI environments";
  };

  config = mkIf config.sadm.roles.sysadmin.enable {
    environment.systemPackages = with pkgs; [
      htop
      killall
      tmux
      tree
      unzip
      vim
    ];

    programs.vim.defaultEditor = true;
    environment.sessionVariables = {
      EDITOR = "vim";
      PAGER = "less";
    };
  };
}
