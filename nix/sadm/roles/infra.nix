{ config, lib, pkgs, ... }:

with lib; {
  options.sadm.roles.infra = {
    enable = mkEnableOption "Infrastructure development related configuration";
  };

  config = mkIf config.sadm.roles.infra.enable {
    environment.systemPackages = with pkgs; [
      colmena
      git
      git-crypt
      kubectl
      kubernetes-helm
      nixfmt
    ];

    virtualisation.docker.enable = true;
  };
}
