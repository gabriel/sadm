{ config, lib, pkgs, ... }:

with lib; {
  options.sadm.roles.yubikey = {
    enable =
      mkEnableOption "Yubikey usage for PAM authentication, GPG and SSH keys";
  };

  config = mkIf config.sadm.roles.yubikey.enable {
    environment.systemPackages = with pkgs; [ yubico-pam yubikey-manager ];

    services.pcscd.enable = true;
    services.udev.packages = [ pkgs.yubikey-personalization ];

    security.pam.yubico = {
      enable = true;
      mode = "challenge-response";
    };

    programs = {
      ssh.startAgent = false;
      gnupg.agent = {
        enable = true;
        enableSSHSupport = true;
      };
    };
  };
}
