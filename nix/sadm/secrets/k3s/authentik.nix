{ lib, ... }:

with lib; {
  options.sadm.k3s.authentik = {
    secretValues = lib.mkOption {
      type = types.path;
      description =
        "Secret values for Authentik helm chart";
      example = "./path/to/authentik.yaml";
    };
  };
}
