{ lib, ... }:

with lib; {
  options.sadm.k3s.argo-cd = {
    secretValues = lib.mkOption {
      type = types.path;
      description =
        "Secret values for Argo CD helm chart";
      example = "./path/to/argo-cd.yaml";
    };
  };
}
