{ lib, ... }:

with lib; {
  options.sadm.k3s.gitea = {
    secretValues = lib.mkOption {
      type = types.path;
      description =
        "Secret values for Gitea helm chart";
      example = "./path/to/gitea.yaml";
    };
  };
}
