{ lib, ... }:

with lib; {
  options.sadm.k3s.minio = {
    secretValues = lib.mkOption {
      type = types.path;
      description =
        "Secret values for Minio helm chart";
      example = "./path/to/minio.yaml";
    };
  };
}
