{ lib, ... }:

with lib; {
  options.sadm.secrets = {
    luksGpgKeyFile = lib.mkOption {
      type = types.path;
      description =
        "GPG encrypted keyfile to unlock the root partition of the machine";
      example = "./path/to/partition-crypt-key.gpg";
    };
  };
}
