{ lib, ... }:

with lib; {
  options.sadm.secrets = {
    userShadowHash = mkOption {
      type = types.str;
      description = "Shadow hash for the unpriviledged user";
      example =
        "$y$j9T$O0ZDifIWQQZ6e4WplpUeB.$N2ExkqNuuUBN5A4Syz9oa3ovNp.FWNHCDhg.h9TEcvC";
    };

    rootShadowHash = mkOption {
      type = types.str;
      default = "!"; # This disables the root user account
      description = "Shadow hash for the root user";
      example =
        "$y$j9T$O0ZDifIWQQZ6e4WplpUeB.$N2ExkqNuuUBN5A4Syz9oa3ovNp.FWNHCDhg.h9TEcvC";
    };
  };
}
