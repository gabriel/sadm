{ lib, ... }:

with lib; {
  options.sadm.assets = {
    userPublicGpgKey = mkOption {
      type = types.path;
      description = "GPG public key path for the unpriviledged user";
      example = "./user-gpg-key.asc";
    };

    userPublicSshKey = mkOption {
      type = types.str;
      description = "SSH public key string for the unpriviledged user";
      example =
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGmF4bAdKLMZG3UA4HwLfnF+zq+tThl2LvJC0ur4560r";
    };
  };
}
