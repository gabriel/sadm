let
  machines = import ./machines;

  makeDeployment = machine: machineModule: {
    imports = [ ./assets ./sadm ./secrets machineModule ];
  };
in builtins.mapAttrs makeDeployment machines
