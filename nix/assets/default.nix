{ lib, ... }:

{
  config = {
    sadm.assets = {
      userPublicGpgKey = ./79C0F6CA9423DB706A7F4E0933F9D0FC001D37B5.asc;
      userPublicSshKey =
        lib.fileContents ./e6vSN15ZESnnaFMz3yJ5JGTwRnwPYFGDQdJ3XerMmlo.pub;
    };
  };
}
