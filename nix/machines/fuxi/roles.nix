{ config, ... }:

let sadm = config.sadm;
in { sadm.roles.k3s.enable = true; }

