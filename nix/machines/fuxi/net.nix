{ name, ... }:

{
  networking.hostName = name;
  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
}
