{ name, config, ... }:

let sadm = config.sadm;
in {
  deployment.targetHost = "${name}.gduque.fr";
  deployment.targetPort = 222;
  deployment.targetUser = "gduque";

  deployment.keys."argo-cd.yaml".keyFile = sadm.k3s.argo-cd.secretValues;
  deployment.keys."argo-cd.yaml".permissions = "0444";

  deployment.keys."authentik.yaml".keyFile = sadm.k3s.authentik.secretValues;
  deployment.keys."authentik.yaml".permissions = "0444";

  deployment.keys."gitea.yaml".keyFile = sadm.k3s.gitea.secretValues;
  deployment.keys."gitea.yaml".permissions = "0444";

  deployment.keys."minio.yaml".keyFile = sadm.k3s.minio.secretValues;
  deployment.keys."minio.yaml".permissions = "0444";
}
