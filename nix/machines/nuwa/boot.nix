{ config, ... }:

let
  secrets = config.sadm.secrets;
  assets = config.sadm.assets;
in {
  boot = {
    initrd = {
      availableKernelModules =
        [ "xhci_pci" "ahci" "nvme" "usb_storage" "sd_mod" "sdhci_pci" ];

      luks = {
        gpgSupport = true;

        devices."nuwa-sys" = {
          device = "/dev/disk/by-partlabel/nuwa-crypt";
          bypassWorkqueues = true;
          gpgCard = {
            publicKey = assets.userPublicGpgKey;
            encryptedPass = secrets.luksGpgKeyFile;
          };
        };
      };
    };

    kernelModules = [ "kvm-intel" ];

    loader = {
      efi.canTouchEfiVariables = true;

      systemd-boot = {
        enable = true;
        editor = false;
        configurationLimit = 5;
      };
    };
  };
}
