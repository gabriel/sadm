{ name, ... }:

{
  networking.hostName = name;
  networking.wireless.enable = true;

  networking.useDHCP = false;
  networking.interfaces.enp1s0.useDHCP = true;
  networking.interfaces.wlp0s20f3.useDHCP = true;

  time.timeZone = "Europe/Paris";
}
