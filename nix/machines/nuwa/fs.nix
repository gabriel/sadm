{
  fileSystems."/" = {
    device = "/dev/disk/by-label/sys";
    fsType = "btrfs";
    options = [ "subvol=@root,compress=zstd,ssd" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/esp";
    fsType = "vfat";
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-label/sys";
    fsType = "btrfs";
    options = [ "subvol=@home,compress=zstd,ssd" ];
  };

  fileSystems."/.snapshots" = {
    device = "/dev/disk/by-label/sys";
    fsType = "btrfs";
    options = [ "subvol=@snapshots,compress=zstd,ssd" ];
  };

  fileSystems."/.swap" = {
    device = "/dev/disk/by-label/sys";
    fsType = "btrfs";
    options = [ "subvol=@swap,compress=zstd,ssd" ];
  };

  swapDevices = [{ device = "/.swap/swapfile"; }];
}

