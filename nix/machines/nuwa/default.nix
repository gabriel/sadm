{
  imports =
    [ ./boot.nix ./deployment.nix ./fs.nix ./hw.nix ./net.nix ./roles.nix ];
  system.stateVersion = "21.11";
}
