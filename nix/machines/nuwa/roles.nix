{ config, ... }:

let sadm = config.sadm;
in {
  sadm.roles.infra.enable = true;
  sadm.roles.x11.enable = true;
  sadm.roles.yubikey.enable = true;
}
