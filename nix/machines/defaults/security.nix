{
  security.sudo.extraRules = [{
    users = [ "gduque" ];
    commands = [{
      command = "ALL";
      options = [ "NOPASSWD" ];
    }];
  }];
}
