{ config, ... }:

let sadm = config.sadm;
in {
  users = {
    mutableUsers = false;
    users = {
      root = {
        hashedPassword = sadm.secrets.rootShadowHash;
        openssh.authorizedKeys.keys = [ sadm.assets.userPublicSshKey ];
      };

      gduque = {
        isNormalUser = true;
        extraGroups = [ "wheel" "docker" ];
        description = "Gabriel Duque";
        home = "/home/gduque";
        hashedPassword = sadm.secrets.userShadowHash;
        openssh.authorizedKeys.keys = [ sadm.assets.userPublicSshKey ];
      };
    };
  };
}
