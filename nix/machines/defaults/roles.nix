{ config, ... }:

let sadm = config.sadm;
in { sadm.roles.sysadmin.enable = true; }
