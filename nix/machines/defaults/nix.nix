{
  nixpkgs.config.allowUnfree = true;
  nix = {
    optimise.automatic = true;
    trustedUsers = [ "gduque" ];
  };
}
