{ lib, config, ... }:

let
  expectedCanaryHash = lib.fileContents ./canary.sha512;
  actualCanaryHash = builtins.hashFile "sha512" ./canary;
in if actualCanaryHash != expectedCanaryHash then
  abort "Secrets are not readable. Have you run `git-crypt unlock`?"
else {
  config = {
    sadm = {
      secrets = {
        userShadowHash = lib.fileContents ./userShadowHash;
        luksGpgKeyFile = ./nuwa-crypt.key.gpg;
      };

      k3s = {
        argo-cd.secretValues = ./argo-cd.yaml;
        authentik.secretValues = ./authentik.yaml;
        gitea.secretValues = ./gitea.yaml;
        minio.secretValues = ./minio.yaml;
      };
    };
  };
}
