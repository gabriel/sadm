let
  base = {
    meta = {
      name = "gduque.fr";
      description = "gduque's network of personal machines";
    };
  };

  deployments = import ./nix;

in base // deployments
